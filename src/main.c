////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2024, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
#include <stm32f1xx_hal.h>

#include "appconfig.h"
#include "can.h"
#include "canmessages.h"
#include "clock.h"
#include "config.h"
#include "ctrl_state.h"
#include "gpio.h"
#include "reset.h"
#include "watchdog.h"

#define BYTES_PER_FLASH_WRITE 8

static void start_application(void){
  void (*app_reset_handler)(void) = (void*)(*((volatile uint32_t*)(APP_START_ADDR + 4U)));
  app_reset_handler();
}

static void erase_flash(uint32_t addr){
  static FLASH_EraseInitTypeDef EraseInitStruct;
  EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
  EraseInitStruct.PageAddress = addr;
  EraseInitStruct.NbPages     = 1;
  uint32_t PAGEError;
  if (HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) != HAL_OK){
    //Error occured while erase flash page. How to react here?
  }
}

static void write_flash_block(uint32_t addr){
  bool has_write_error=false;
  //We write BYTES_PER_FLASH_WRITE wise
  for(uint16_t i=0;i<FLASH_BLOCK_SIZE/BYTES_PER_FLASH_WRITE;i++){
    if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, addr+(i*BYTES_PER_FLASH_WRITE), ctrl_state_flash_data.data_uint64[i]) != HAL_OK){
      //Error occured
      has_write_error=true;
    }
  }
  //No clue how to react on write error
  (void)has_write_error;
}

static void bootloader_task(void){
  static uint32_t blocks_flashed=0;
  static bool initial_flashed=false;

  //watchdog_serve();  //We don't use watchdog yet.
  canmessages_rx_handler();
  if(ctrl_state_flush_to_flash==true){
    //Write received program data to flash
    uint32_t address_to_flash=APP_START_ADDR;
    address_to_flash+=blocks_flashed*FLASH_BLOCK_SIZE;
    HAL_FLASH_Unlock();
    if((address_to_flash%FLASH_PAGE_SIZE==0)||(initial_flashed==false)){
      //We need to erase the sector
      erase_flash(address_to_flash);
      initial_flashed=true;
    }
    write_flash_block(address_to_flash);

    ++blocks_flashed;

    //Clean up
    ctrl_state_clear_flash_data();
    HAL_FLASH_Lock();
    canmessage_ack.content.response=ctrl_state_received_data_msg_count;
    canmessage_ack.content.addr=address_to_flash;
    //Direct CAN send
    can_send_message(MSG_ACK_ID,MSG_ACK_DLC,canmessage_ack.data);
    ctrl_state_flush_to_flash=false;
  }
  if(ctrl_state_data_transfer_started==false){
    blocks_flashed=0;
  }
}

int main(void){
  HAL_Init();
  reset_init(false);
  config_init();
  if(((reset_get_dr1_value()&BACKUP_DR1_BL_REQUESTED) == 0)&&
     ((config_calculate_app_crc()==config_get_stored_crc())&&
      config_get_stored_crc()!=0xFFFF)
      )
  {
    start_application();
  }
  else{
    //We want to do bootloader stuff
    SystemClock_Config();
    gpio_init();
    can_init();
    canmessages_init();
    ctrl_state_init();
#if USE_WATCHDOG == 1
    watchdog_init();
#endif
    canmessages_send_status_message();
    while(ctrl_state_start_app_requested==false){
#if USE_WATCHDOG == 1
      watchdog_serve();
#endif
      bootloader_task();
    }
    //No bootloader task any more. Now try to start application.
    config_write();
    //Set application start request
    reset_set_dr1_value(reset_get_dr1_value()&(~BACKUP_DR1_BL_REQUESTED));
    reset_by_soft_reset();
  }
}
