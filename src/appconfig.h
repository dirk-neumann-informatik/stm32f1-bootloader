////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2024, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
//
//Application configuration

#pragma once

#define SW_VERSION_MAJOR 2  //2 = STM32 Bootloader
#define SW_VERSION_MINOR 1  //1 = STM32F103 with compressed flash layout

//==========================================================
// Board selection
//==========================================================

#define USE_BOARD_BLUEPILL     1

#if USE_BOARD_BLUEPILL != 1
#error "Thou shall select exactly one boarde"
#endif

//==========================================================
// Features
//==========================================================
// Shall the watchdog (IWDG) functionality be used?
#define USE_WATCHDOG           1
//==========================================================
// Clock configuration
//==========================================================
//We use only internal clock.
//Define, how many MHz are wished to be configured by SystemInit
//#define CLOCK_INIT_SPEED_MHZ 4
#define CLOCK_INIT_SPEED_MHZ 8
//#define CLOCK_INIT_SPEED_MHZ 16
//#define CLOCK_INIT_SPEED_MHZ 24
//#define CLOCK_INIT_SPEED_MHZ 32
//#define CLOCK_INIT_SPEED_MHZ 64

//==========================================================
// CAN Message config part.
//
//==========================================================

//Status
#define MSG_STATUS_ID 0x08E
#define MSG_STATUS_DLC 8
//Ack
#define MSG_ACK_ID 0x08F
#define MSG_ACK_DLC 8

//Config
#define MSG_COMMAND_ID 0x07E
#define MSG_COMMAND_DLC 8
//Data
#define MSG_DATA_ID 0x07F
#define MSG_DATA_DLC 8

//==========================================================
// Flash addresses config part.
// We use the flash layout with page size 1024
// Content     Address      Page
// BL Text     0x0800000      0
// BL Text     0x0800400      1
// BL Text     0x0800800      2
// BL Text     0x0800C00      3
// BL Text     0x0801000      4
// BL Config   0x0801400      5
// App Text    0x0801600      5 -> end
//
//==========================================================
#define BOOTLOADER_CONFIG_PAGE_ADDR 0x08001400
#define BOOTLOADER_CONFIG_PAGE_AREA_SIZE 512 //We don't need 512, but this is the minimal VECT_TAB_OFFSET for STM32F103
#define APP_START_ADDR BOOTLOADER_CONFIG_PAGE_ADDR+BOOTLOADER_CONFIG_PAGE_AREA_SIZE
#define FLASH_END_ADDR 0x08100000   //These are 1024KB
