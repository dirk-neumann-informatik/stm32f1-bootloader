////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2023, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
//
// This is the list of CAN messages.
//

#pragma once
#include <stdint.h>
#include <stdbool.h>
#include "appconfig.h"
#include <stm32f1xx_hal_can.h>

typedef union{
  uint8_t data[MSG_STATUS_DLC];
  struct __attribute__((packed, scalar_storage_order("big-endian"))){
    uint8_t bl_version_major;
    uint8_t bl_version_minor;
    uint32_t app_end_addr;
    uint16_t crc;
  }content;
}canmessage_status_t;

typedef union{
  uint8_t data[MSG_ACK_DLC];
  struct __attribute__((packed, scalar_storage_order("big-endian"))){
    uint32_t response;
    uint32_t addr;
  }content;
}canmessage_ack_t;

typedef union{
  uint8_t data[MSG_COMMAND_DLC];
  struct __attribute__((packed, scalar_storage_order("big-endian"))){
    uint8_t config_command;
    uint8_t reserved[7];
  }content;
  struct __attribute__((packed, scalar_storage_order("big-endian"))){
    uint8_t config_command;
    uint32_t app_param;
    uint8_t reserved[3];
  }app_content;
  struct __attribute__((packed, scalar_storage_order("big-endian"))){
    uint8_t config_command;
    uint16_t app_crc;
    uint8_t reserved[5];
  }crc_content;
}canmessage_command_t;

typedef union{
  uint8_t data[MSG_DATA_DLC];
}canmessage_data_t;

typedef union{
  uint8_t data[8];
}canmessage_rx_t;

// This is the control struct for RX-messages. It is used as an array
// entry for the cyclic RX handler.
typedef struct{
  uint32_t id;
  uint8_t dlc;
  uint8_t* data;
  bool handled;
}canmessage_rx_config;

extern canmessage_status_t canmessage_status;
extern canmessage_ack_t canmessage_ack;
extern canmessage_command_t canmessage_command;
extern canmessage_data_t canmessage_data;

void canmessages_init(void);
void canmessages_rx_store(CAN_RxHeaderTypeDef rx_header, uint8_t *rx_data);
void canmessages_rx_handler(void);
void canmessages_send_status_message(void);
