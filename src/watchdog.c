////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2024, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
//
// Watchdog functions.
//

#include "watchdog.h"

#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_rcc.h>
#include <stm32f1xx_hal_iwdg.h>

#if USE_WATCHDOG == 1

IWDG_HandleTypeDef hiwdg;

void watchdog_init(void){
  //LSI is turned on automatically
  hiwdg.Instance=IWDG;
  //hiwdg.Init.Prescaler=IWDG_PRESCALER_4;  //=32 kHz/4 = 125µs
  hiwdg.Init.Prescaler=IWDG_PRESCALER_32;  //=32 kHz/32 = 1ms
  //hiwdg.Init.Prescaler=IWDG_PRESCALER_64;  //=32 kHz/64 = 2ms
  //hiwdg.Init.Prescaler=IWDG_PRESCALER_128;  //=32 kHz/128 = 4ms
  //hiwdg.Init.Prescaler=IWDG_PRESCALER_256;  //=32 kHz/256 = 8ms
  //This parameter must be a number between 0 and 4095.
  hiwdg.Init.Reload=4095;  //=4s
  HAL_IWDG_Init(&hiwdg);
}

// The watchdog service function has to be called cyclic to prevent
// a watchdog reset.
void watchdog_serve(void){
  HAL_IWDG_Refresh(&hiwdg);
}

void watchdog_force_reset(void){
  while(1);
}

#endif
