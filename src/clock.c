////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2024, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
#include <stdbool.h>
#include "appconfig.h"
#include <stm32f1xx_hal.h>
#include <stm32f1xx_ll_rcc.h>
#include <stm32f1xx_ll_system.h>
#include <stm32f1xx_ll_utils.h>

void SystemClock_Config(void){
#if CLOCK_INIT_SPEED_MHZ == 64
  #define M_PLL_MULTIPLIER LL_RCC_PLL_MUL_16
  #define M_AHB_PRESCALER LL_RCC_SYSCLK_DIV_1
  #define M_APB1_PRESCALER LL_RCC_APB1_DIV_2
  #define M_FLASH_LATENCY LL_FLASH_LATENCY_2
#elif CLOCK_INIT_SPEED_MHZ == 32
  #define M_PLL_MULTIPLIER LL_RCC_PLL_MUL_8
  #define M_AHB_PRESCALER LL_RCC_SYSCLK_DIV_1
  #define M_APB1_PRESCALER LL_RCC_APB1_DIV_2
  #define M_FLASH_LATENCY LL_FLASH_LATENCY_1
#elif CLOCK_INIT_SPEED_MHZ == 24
  #define M_PLL_MULTIPLIER LL_RCC_PLL_MUL_6
  #define M_AHB_PRESCALER LL_RCC_SYSCLK_DIV_1
  #define M_APB1_PRESCALER LL_RCC_APB1_DIV_2
  #define M_FLASH_LATENCY LL_FLASH_LATENCY_1
#elif CLOCK_INIT_SPEED_MHZ == 16
  #define M_PLL_MULTIPLIER LL_RCC_PLL_MUL_4
  #define M_AHB_PRESCALER LL_RCC_SYSCLK_DIV_1
  #define M_APB1_PRESCALER LL_RCC_APB1_DIV_2
  #define M_FLASH_LATENCY LL_FLASH_LATENCY_0
#elif CLOCK_INIT_SPEED_MHZ == 8
  #define M_PLL_MULTIPLIER LL_RCC_PLL_MUL_2
  #define M_AHB_PRESCALER LL_RCC_SYSCLK_DIV_1
  #define M_APB1_PRESCALER LL_RCC_APB1_DIV_1
  #define M_FLASH_LATENCY LL_FLASH_LATENCY_0
#elif CLOCK_INIT_SPEED_MHZ == 4
  #define M_PLL_MULTIPLIER LL_RCC_PLL_MUL_2
  #define M_AHB_PRESCALER LL_RCC_SYSCLK_DIV_2
  #define M_APB1_PRESCALER LL_RCC_APB1_DIV_1
  #define M_FLASH_LATENCY LL_FLASH_LATENCY_0
#endif
  LL_FLASH_SetLatency(M_FLASH_LATENCY);
  while(LL_FLASH_GetLatency()!=M_FLASH_LATENCY){
  }
  LL_RCC_HSI_GetCalibTrimming();
  LL_RCC_HSI_Enable();
  while(LL_RCC_HSI_IsReady()!=1){
  }
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI_DIV_2,M_PLL_MULTIPLIER);
  LL_RCC_PLL_Enable();
  while(LL_RCC_PLL_IsReady()!=1);

  LL_RCC_SetAHBPrescaler(M_AHB_PRESCALER);
  LL_RCC_SetAPB1Prescaler(M_APB1_PRESCALER);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
  while(LL_RCC_GetSysClkSource()!=LL_RCC_SYS_CLKSOURCE_STATUS_PLL){
  }
  LL_SetSystemCoreClock(CLOCK_INIT_SPEED_MHZ*1000000);
}
