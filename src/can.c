////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2024, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
#include "can.h"

#include <stm32f1xx_hal_can.h>
#include <stm32f1xx_hal_cortex.h>
#include <stm32f1xx_hal_rcc.h>
#include <stm32f1xx_hal_rcc_ex.h>
#include <stm32f1xx_ll_bus.h>
#include <stm32f1xx_ll_rcc.h>

#include "canmessages.h"

CAN_HandleTypeDef hcan1;

typedef struct{
  uint32_t brp;
  uint32_t bs1;
  uint32_t bs2;
}canhelper_prescaler;

// http://www.bittiming.can-wiki.info/
// CAN is a APB1 peripheral, it runs with half of MCU clock (at 8 MHz and less
// full MCU clock), see clock tree in reference manual. Clock source is PLL.
// We want to get 500 kbit/s.
#if CLOCK_INIT_SPEED_MHZ == 4
const canhelper_prescaler m_prescaler={1,CAN_BS1_6TQ,CAN_BS2_1TQ};
#elif CLOCK_INIT_SPEED_MHZ == 8
const canhelper_prescaler m_prescaler={2,CAN_BS1_6TQ,CAN_BS2_1TQ};    //Calculator 2. Variante
#elif CLOCK_INIT_SPEED_MHZ == 16
const canhelper_prescaler m_prescaler={1,CAN_BS1_8TQ,CAN_BS2_7TQ};
#elif CLOCK_INIT_SPEED_MHZ == 24
const canhelper_prescaler m_prescaler={2,CAN_BS1_10TQ,CAN_BS2_1TQ};
#elif CLOCK_INIT_SPEED_MHZ == 32
const canhelper_prescaler m_prescaler={2,CAN_BS1_8TQ,CAN_BS2_7TQ};
#elif CLOCK_INIT_SPEED_MHZ == 64
const canhelper_prescaler m_prescaler={4,CAN_BS1_8TQ,CAN_BS2_7TQ};
#elif CLOCK_INIT_SPEED_MHZ == 72
const canhelper_prescaler m_prescaler={4,CAN_BS1_15TQ,CAN_BS2_2TQ};
#endif

void can_init(void){
  // CAN peripheral clock enable
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_CAN1);

  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler=m_prescaler.brp;
  hcan1.Init.TimeSeg1=m_prescaler.bs1;
  hcan1.Init.TimeSeg2=m_prescaler.bs2;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = ENABLE;
  hcan1.Init.AutoWakeUp = ENABLE;
  hcan1.Init.AutoRetransmission = ENABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;

  HAL_CAN_DeInit(&hcan1);
  HAL_CAN_Init(&hcan1);
  HAL_CAN_Start(&hcan1);

  // set RX filters
  CAN_FilterTypeDef sFilterConfig;
  // We want to get all 07[E|F] messages
  uint16_t single_id=0x007E;

  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilterConfig.FilterIdHigh = single_id<<5;
  sFilterConfig.FilterIdLow = 0x0000;
  sFilterConfig.FilterMaskIdHigh = 0x07FE<<5;  //7FE=Don't care the last bit
  sFilterConfig.FilterMaskIdLow = 0x0000;
  sFilterConfig.FilterFIFOAssignment = 0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.FilterBank = 0;

  HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig);

  HAL_NVIC_SetPriority(CAN1_RX0_IRQn,0,0);
  HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
  uint32_t active_interrupts=0;
  active_interrupts|=CAN_IT_RX_FIFO0_MSG_PENDING;
  active_interrupts|=CAN_IT_RX_FIFO0_FULL;
  active_interrupts|=CAN_IT_RX_FIFO0_OVERRUN;
  active_interrupts|=CAN_IT_RX_FIFO1_MSG_PENDING;
  active_interrupts|=CAN_IT_RX_FIFO1_FULL;
  active_interrupts|=CAN_IT_RX_FIFO1_OVERRUN;
  HAL_CAN_ActivateNotification(&hcan1, active_interrupts);
}

bool can_send_message(uint32_t id,uint8_t dlc,uint8_t *data){
  bool retval = false;
  HAL_StatusTypeDef status = HAL_ERROR;

  CAN_TxHeaderTypeDef tx_header;
  tx_header.DLC=dlc;
  tx_header.ExtId=0;
  tx_header.StdId=id;
  tx_header.IDE=CAN_ID_STD;
  tx_header.RTR=CAN_RTR_DATA;

  uint32_t tx_mailbox;

  status = HAL_CAN_AddTxMessage(&hcan1,&tx_header,data,&tx_mailbox);

  if(status != HAL_ERROR){
    retval=true;
  }
  return retval;
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan){
  CAN_RxHeaderTypeDef rx_header;
  uint8_t rx_data[8];

  HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &rx_header, rx_data);
  //Now store message
  canmessages_rx_store(rx_header,rx_data);
}

void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan){
  CAN_RxHeaderTypeDef rx_header;
  uint8_t rx_data[8];
  HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO1, &rx_header, rx_data);
  //Now store message
  canmessages_rx_store(rx_header,rx_data);
}
