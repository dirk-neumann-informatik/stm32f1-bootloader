////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2024, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
//
// GPIO configuration. This file is mainly used as an overview to prevent pin assignment collisions.
// If you want to use pins in a functional module, you have to mention the port and pin number there.
//

#include "gpio_bluepill.h"

#if USE_BOARD_BLUEPILL == 1

#include "stm32f1xx_ll_bus.h"
#include <stm32f1xx_ll_gpio.h>

void gpio_bluepill_init(void){
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_AFIO);
  LL_GPIO_SetPinMode(CAN_RX_PORT, CAN_RX_PIN, LL_GPIO_MODE_INPUT);
  LL_GPIO_SetPinOutputType(CAN_RX_PORT, CAN_RX_PIN,LL_GPIO_OUTPUT_PUSHPULL);
  LL_GPIO_SetPinMode(CAN_TX_PORT, CAN_TX_PIN, LL_GPIO_MODE_ALTERNATE);
  LL_GPIO_SetPinOutputType(CAN_TX_PORT, CAN_TX_PIN,LL_GPIO_OUTPUT_PUSHPULL);
}
#endif  //USE_BOARD_BLUEPILL == 1
