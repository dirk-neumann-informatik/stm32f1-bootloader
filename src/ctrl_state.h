////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2024, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
//
// This is the control state. The state is set and checked by various actors.
//

#pragma once

#include <stdbool.h>
#include <stdint.h>

//We want to write 128 Bytes at once. This is not the size of a flash sector.
#define FLASH_BLOCK_SIZE 128

typedef union{
  uint8_t data_uint8[FLASH_BLOCK_SIZE];
  uint16_t data_uint16[FLASH_BLOCK_SIZE/2];
  uint32_t data_uint32[FLASH_BLOCK_SIZE/4];
  uint64_t data_uint64[FLASH_BLOCK_SIZE/8];
}ctrl_state_flash_data_t;

extern volatile bool ctrl_state_start_app_requested;
extern volatile bool ctrl_state_data_transfer_started;
extern volatile bool ctrl_state_flush_to_flash;
extern volatile uint16_t ctrl_state_received_data_msg_count;

extern volatile ctrl_state_flash_data_t ctrl_state_flash_data;

void ctrl_state_init(void);
void ctrl_state_clear_flash_data(void);
