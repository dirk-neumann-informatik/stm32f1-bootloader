////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2024, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
//
// Bootloader configuration functions.
//

#include "appconfig.h"

#include <stddef.h>
#include <stm32f1xx_hal_flash.h>
#include "config.h"

#define CONFIG_SIZE 1

typedef union{
  uint8_t data[CONFIG_SIZE*8];
  uint64_t data64[CONFIG_SIZE];
  struct{
    uint32_t app_end_addr;
    uint16_t app_crc;
    uint16_t reserved;
  }content;
}bootloader_config_t;

bootloader_config_t m_config ={0};
bootloader_config_t m_orig_config = {0};

void config_init(void){
  //Read data from flash
  uint32_t read_addr=BOOTLOADER_CONFIG_PAGE_ADDR;
  for(uint8_t i=0;i<CONFIG_SIZE;i++){
    m_config.data64[i]=*((volatile uint64_t*)(read_addr+(i*8)));
  }
  m_orig_config=m_config;
}

void config_set_app_crc(uint16_t crc){
  m_config.content.app_crc=crc;
}

uint16_t config_get_stored_crc(void){
  return m_config.content.app_crc;
}

uint32_t config_get_app_end_address(void){
  return m_config.content.app_end_addr;
}

void config_set_app_end_address(uint32_t addr){
  m_config.content.app_end_addr=addr;
}

uint16_t config_calculate_app_crc(void){
  //We need to calculate the crc with method CRC-CCITT over the flash memory,
  //starting at APP_START_ADD until APP_END_ADDR.
  //https://www.lammertbies.nl/comm/info/crc-calculation

  uint8_t x;
  uint16_t crc=0xFFFF;
  uint32_t flash_start_addr=APP_START_ADDR;
  uint32_t flash_end_addr=m_config.content.app_end_addr;
  uint32_t current_flash_addr;
  uint8_t current_byte;

  if((flash_end_addr<=FLASH_END_ADDR)&&(flash_end_addr>flash_start_addr)){
    for(current_flash_addr=flash_start_addr;
        current_flash_addr<=flash_end_addr;++current_flash_addr){
      //Read byte from flash
      current_byte=(*(volatile uint8_t*)(current_flash_addr));
      x = crc >> 8 ^ current_byte;
      x ^= x>>4;
      crc = (crc << 8) ^ ((uint16_t)(x << 12)) ^ ((uint16_t)(x <<5)) ^ ((uint16_t)x);
    }
  }
  return crc;
}

void config_write(void){
  // Config write new style: Only the first 128 Bytes of the config sector are
  // used for config, other bytes are program code
  // Config match? -> No write
  // Config mismatch and Config area empty? -> write
  // Config mismatch and Config area not empty? -> erase and write

  //Check for config in Flash
  //Read data from flash
  uint32_t read_addr=BOOTLOADER_CONFIG_PAGE_ADDR;
  bootloader_config_t flash_config;
  for(uint8_t i=0;i<CONFIG_SIZE;i++){
    flash_config.data64[i]=*((volatile uint64_t*)(read_addr+(i*8)));
  }
  //Config match?
  bool is_config_changed=false;
  for(uint8_t i=0;i<CONFIG_SIZE;i++){
    if(m_config.data64[i]!=flash_config.data64[i]){
      is_config_changed=true;
    }
  }
  //Config area empty?
  bool is_config_area_empty=true;
  for(uint8_t i=0;i<CONFIG_SIZE;i++){
    if(flash_config.data64[i]!=0xFFFFFFFFFFFFFFFF){
      is_config_area_empty=false;
    }
  }
  //Check for erase
  if((is_config_area_empty==false)&&(is_config_changed==true)){
    //Erase config page.
    HAL_FLASH_Unlock();
    //Erase 1 page
    static FLASH_EraseInitTypeDef EraseInitStruct;
    EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
    EraseInitStruct.PageAddress = BOOTLOADER_CONFIG_PAGE_ADDR;
    EraseInitStruct.NbPages     = 1;
    uint32_t PAGEError;
    if (HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) == HAL_OK){
      //No error occured while erase flash page
      is_config_area_empty=true;
    }
    HAL_FLASH_Lock();
  }
  //Check for write
  if((is_config_changed==true)&&(is_config_area_empty==true)){
    //write config needed and possible
    HAL_FLASH_Unlock();
    for(uint8_t i=0;i<CONFIG_SIZE;i++){
      if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, BOOTLOADER_CONFIG_PAGE_ADDR+(i*8), m_config.data64[i])!=HAL_OK){
        //Error occured on write. How to react?
      }
    }
    HAL_FLASH_Lock();
  }
}
