## Placement
Some time ago, I wrote a CAN bootloader for the STM8AF:
https://gitlab.com/heckenrose/stm8af-bootloader
Here I modified that program for the use with STM32F1. The reasons are the same
like they were in the past. Also this time there is a CAN bootloader provided
by ST:
https://www.st.com/resource/en/application_note/an3154-can-protocol-used-in-the-stm32-bootloader-stmicroelectronics.pdf
And again, this bootloader had some problems:
* It needs a HSE clock source
* It needs a low speed capable CAN transceiver

## Flash layout
The user flash is divided into 3 parts:

1. The bootloader
2. The bootloader configuration page
3. The main application segment

The main application may separate its segment further, but this is not relevant
for the bootloader.

The bootloader itself resides at the first 5 KB, these are the addresses
`0x08000000`..`0x080013FF`. As the bootloader uses in fact 4312 bytes in flash,
one may find other 216 bytes to save, then we could restrict the bootloader
program area to the first 4 KB, meaning 1 extra KB for main application.

As the STM32F103 has a flash page size of 1024 Bytes, we do not want to use
a complete flash page for the bootloader configuration bytes. So we store the
bootloader config in the first part of the first application sector and move
the application start address by some bytes. As the minimal Vector table offset
of the STM32F103 is `0x200`, which is 512 bytes, we use the first 512 bytes for
the bootloader configuration, which means addresses `0x08001400..0x080015FF`.
Only the first 6 bytes are used, so we waste 506 Bytes. But this is less than
nearly 1 K for a whole flash page.

The main application segment is the rest of the MCU flash, starting at `0x08001600.`

```
       +------------------+
       |                  |
       | Main application |
5.5 KB +------------------+ 0x08001600
       | BL config area   |
5 KB   +------------------+ 0x08001400
       | Bootloader       |
0 KB   +------------------+ 0x08000000
```

### Bootloader config page

|  Offset  | Length |  Content                |
| -------- | ------ | ----------------------- |
| `0x0000` |  4     | Application end address |
| `0x0004` |  2     | Application CRC         |

* Application end address: 32 Bit, Little Endian
* Application CRC: 16 Bit, CCITT, Little Endian

## Bootup
To let the main application communicate with the bootloader, we need to store
information which survises a soft reboot. Therefore, ST made some backup data
registers, we use the DR1 to store a bit (we use the lowest bit of DR1) to
signalize the bootloader, that the application got an update request.

As the bootloader resides at the STM32 user flash start address, it is started
as the first thing after reset. OK, on STM32 there are some other specialities
like booting from RAM etc., but if you are able to cope with these boot modes,
you do not need to look into this documentation ;)

The bootloader examines the lowest bit of Backup Data register 1. If this bit is
set to 0, it indicates that there is no request to flash a new program; instead,
the bootloader's task is to initiate the existing main application. To do this,
the segment is scrutinized to verify whether it possesses the same CRC as stored
in the bootloader's configuration page. If a match is found, the main application
is launched. In the event that the CRC does not align, the bootloader remains in
standby mode, awaiting commands through the CAN bus.
This leads us to the general bootup sequence:

1. Is Backup Data register, bit 0 == `1`? If yes, jump over to 3.
2. Is the CRC of the app area correct? If yes, jump over to 5.
3. Initialize CAN and wait for commands
4. If command == `0x04`: Is CRC of app area correct? If yes jump over to 5.
5. Start application by jumping to app start address.

This needs again some cooperation with application. If your main application
finds, that it should be updated (feel free to command like you want), then
it has to set the Bit 0 of backup data register 1 to `1`. Subsequently, when the
bootloader initiates the application, it is responsible for resetting this bit
to `0`.

## Interrupt vector relocation
The STM32 has a vector table offset register which contains the start address
of the interrupt vector table. To skip the background reading, we can configure
it by setting the `VECT_TAB_OFFSET` in our `system_stm32f1xx.c` to the main
application startup address. If you are interested in the background, you can
read here:
https://embetronicx.com/tutorials/microcontrollers/stm32/reset-sequence-in-arm-cortex-m4/
This makes it easier than with the STM8, because we do not have to relocate
each interrupt ourselves using MCU cycles.

## Scheduling
The bootloader does not utilize timer interrupts, as the STM8 variant also abstains
from doing so, and I wanted to avoid making unnecessary alterations. Instead,
it operates solely based on CAN RX interrupts, promptly reacting to each incoming
CAN message. When a sufficient number of data messages are received, they are
aggregated into a block and written to flash memory. Any other incoming commands
are also handled.

## CAN messages

### RX
* `0x07E` Config/Command
* `0x07F` Data

#### `0x07E` Config/Command
The first byte is the command, followed by parameters.

| Command         | Meaning                       | Parameters                                |
| --------------- | ----------------------------- | ----------------------------------------- |
| `0x02 ABCDEF01` | set app end address           | `0xABCDEF01`: address, 32 Bit, Big Endian |
| `0x03 ABCD`     | set app CRC                   | `0xABCD`: CRC, 16 Bit, Big Endian         |
| `0x04`          | Quit Bootloader and start app | none                                      |
| `0x05`          | Request status message        | none                                      |
| `0x06`          | Start data transfer           | none                                      |
| `0x07`          | End data transfer             | none                                      |


Some contemplation about commands:
* `0x04`: If the application cannot be started (e.g. CRC mismatch), we restart in BL mode.
* `0x05`: The status message is sent as response to this request.


#### `0x07F` Data
A data message contains up to 8 data bytes. The order is rising. After 16 messages,
the bootloader writes the block (128 bytes) into flash and responds with ACK
message, if successful. The programming host therefore has to wait after 16
data messages to receive an ACK message. 

If there is not enough data to fill 128 bytes, the programming host has to shorten
the last data message to the number of bytes left (e.g. 3) and then send a command
`End data transfer`. The bootloader writes the bytes into flash and sends an ACK
message. 


### TX
* `0x8E` Status
* `0x8F` ACK

#### `0x8E` Status

```
+---------+---------+---------+---------+---------+---------+---------+---------+
| Byte 0  | Byte 1  | Byte 2..5                             | Byte 6..7         |
+---------+---------+---------+---------+---------+---------+---------+---------+
| Ver.Maj | Ver.Min | App end address                       | App CRC           |
+---------+---------+---------+---------+---------+---------+---------+---------+
```

* Ver.Maj is the major bootloader version. I use 2 here to show that it is STM32
  bootloader
* Ver.Min is the minor bootloader version.
* APP CRC is the calculated CRC over the main application.

#### `0x8F` ACK
```
+---------+---------+---------+---------+---------+---------+---------+---------+
| Byte 0..3                             | Byte 4..7                             |
+---------+---------+---------+---------+---------+---------+---------+---------+
| Data messages received                | Flash block start address             |
+---------+---------+---------+---------+---------+---------+---------+---------+
```

## Message time
At bootloader start, it sends a single CAN status message, if the application
is not started.

## Size of bootloader
Currently the app start address is `0x08001600`, meaning the bootloader has got
5 KB of user flash memory. But in fact, it uses 4312 bytes in the bootloader segment
and 6 bytes in the bootloader configuration page.
I use the LL-API instead of HAL-API where possible, because this reduces footprint
in Text segment by 400 Bytes.

## Evironment
This bootloader is tested on STM32F103C8T6 on Bluepill Board and on a custom
board based on STM32F103CBT6.

## Compiling
This bootloader has been successfully compiled and tested on a Debian Bookworm
machine using gcc-arm-none-eabi version 12.2.1, along with cmake and make, all
of which were installed from the standard Debian package repository. The following
steps can be followed:

```
mkdir build
cd build
cmake ..
make
```

This builds a file called bootloader_stm32f103.hex, which can be flashed to target
via STLink, e.g. by using OpenOCD.

## Speed considerations
The MCU runs at 8 MHz, and using a 2 MHz clock is perfectly acceptable. Although
it can potentially reach a clock speed of 64 MHz, it's important to understand 
that there won't be a significant speed increase due to the bottleneck in CAN communication.
Only in case of main application start, the MCU clock has some effect, I measured
the CRC check time for a 16 KB main application. I also measured the complete update
process of a 16 KB main application, using a 5 µs (yes, µs, not ms!) CAN message
period on host machine.

| Clock  | CRC Calculation time | Complete update time |
| -------| --------------------:| --------------------:|
| 8 MHz  | 17.8 ms              |   1.056s             |
| 24 MHz |  6.8 ms              |   1.047s             |
| 64 MHz |  3.2 ms              |   1.040s             |

Only if you need to save some milliseconds in bootup, especially if you have a
larger main application, you should take a clock increase into account.
